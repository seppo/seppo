(** https://www.w3.org/TR/activitypub/#retrieving-objects *)
module ContentType = struct
  let _app_act_json = "application/activity+json" (* can we phase this out in favour of app_jlda *)
  let app_atom_xml = "application/atom+xml"
  let _app_ld_json  = "application/ld+json"
  (* https://macgirvin.com/item/67716c6f-1226-4a10-8c04-cc7a70a24cbc *)
  let app_jlda     = {|application/ld+json; profile="https://www.w3.org/ns/activitystreams"|}
  let app_jrd      = "application/jrd+json"
  let app_json     = "application/json" (* mostly webfinger rfc7033 jrd only *)
  let text_html    = "text/html"
  let text_xml     = "text/xml"
  let any = "*/*"

  let content_types = [
    _app_act_json,   `JSON;
    app_atom_xml,    `ATOM;
    _app_ld_json,    `JSON;
    app_jlda,        `JSON;
    app_jrd,         `JSON;
    app_json,        `JSON;
    text_html,       `HTML;
    text_xml,        `XML;
    any,             `HTML;
  ]

(*
  let of_string content_type =
    List.find_opt
      (fun (str, _) ->
         String.prefix ~pre:str content_type) content_types
    |> Option.map snd
*)

end

(** https://datatracker.ietf.org/doc/html/rfc7033 *)
module Webfinger = struct
  let json_rd      = "application/jrd+json"

  let self_rel     = "self"
  let ostatus_rel  = "http://ostatus.org/schema/1.0/subscribe"
  let profile_page = "http://webfinger.net/rel/profile-page"
  let alternate    = "alternate"
end

(** https://www.w3.org/TR/activitystreams-core/ *)
module ActivityStreams = struct
  let ns_as  = "https://www.w3.org/ns/activitystreams"
  let ns_sec = "https://w3id.org/security/v1"
  let public = Some "Public" |> Uri.with_fragment (ns_as |> Uri.of_string)

  let und = Some "und"

  let context lang =
    "@context", `A [
      `String ns_as;
      `String ns_sec;
      `O [
        (* https://docs.joinmastodon.org/spec/activitypub/#schema *)
        ("schema",        `String "http://schema.org#");
        ("PropertyValue", `String "schema:PropertyValue");
        ("value",         `String "schema:value");
        ("@language",     `String lang); (* undefined. BCP47 https://www.w3.org/TR/activitystreams-core/#naturalLanguageValues *)
      ]
    ]
end
