
# v$ver, $date

Binary:   seppo.cgi
Source:   source.tar.gz
Download: https://Seppo.mro.name/en/downloads/seppo-$ver
Git:      https://Seppo.mro.name/v/$GIT_SHA
Install:  https://seppo.mro.name/en/support/#installation

Changes

v0.5

- allow cron to trigger queue flush
- allow other actors than Person
- slowly phase out seppo.social in favour of seppo.mro.name
- Announce
- Like
- enable to recreate entries
- a11y / UX audit

v0.4

- security audit
- un/follow, block
- display inbox notes
- support /cgi-bin/ webspace
- delete posts
- breaking on disk changes (themes, .s)
- apchk.cgi brushed up
- follower list
- deny follower
- publicKey.owner property optional
- html/css/js tweaks
- app/var/run/cookie.sec persistence format
- app/var/db/notify.cdb persistence format
- various commandline helpers (seppo.cgi -h)
- https optional https://seppo.mro.name/issues/8

Verify the signature with openssl:

$ curl -LO https://Seppo.mro.name/seppo.pub.pem
$ openssl dgst -verify seppo.pub.pem -keyform PEM -sha256 -signature seppo.cgi.signature -binary seppo.cgi
