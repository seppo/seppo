
cpu	:= $(shell uname -m)
os	:= $(shell uname -s)
ver	:= $(shell grep -hoE "\(version +[^ \)]+" dune-project | cut -d " " -f2 | head -1)
ver	:= $(ver)-alpha+$(shell date +%Y%m%d)
dst := _build/seppo-$(os)-$(cpu)-$(ver).cgi
cver := 0.1
cdst := _build/apchk-$(os)-$(cpu)-$(cver).cgi

# %.ttl: %.rdf
#	rapper -i rdfxml -o turtle $< > $@

%.rdf: %.ttl
	rapper -i turtle -o rdfxml-abbrev $< > $@

final: build $(dst) $(cdst) doap.rdf

pull:
	git pull

deps:
	opam install . --deps-only --with-test --with-doc --yes

deploy: final
	rsync -avPz $(dst) $(cdst) seppo.mro.name:~/seppo.mro.name/_bin/`date +%FT%H%M%S`/

$(dst): _build/default/bin/seppo_bin.exe
	cp $< $@
	chmod u+w $@
	strip $@
	ls -l $@

$(cdst): _build/default/chkr/apchk.exe
	cp $< $@
	chmod u+w $@
	strip $@
	ls -l $@

test/assert.ml:
	curl --location --output $@ https://raw.githubusercontent.com/benjenkinsv95/ocaml-unit-testing-helpers/master/assert.ml

#
# https://github.com/ocaml/dune/tree/master/example/sample-projects/hello_world
# via https://stackoverflow.com/a/54712669
#
.PHONY: all deps pull build clean test doc

build:
	dune build --profile release bin/seppo_bin.exe
	dune build --profile release chkr/apchk.exe

all: build

test: test/assert.ml
	dune runtest

doc: _build/post.svg _build/auth.svg
	dune build @doc

clean:
	# rm -rf _build *.install
	dune clean

_build/%.svg: %.msc
	mscgen -T svg -o $@ -i $^

_build/%.svg: %.dot
	dot -Tsvg $^ > $@

_build/%.pdf: %.dot
	dot -Tpdf $^ > $@
