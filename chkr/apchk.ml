(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let () =
  let module Cg = Seppo_lib.Cgi in
  let module L = Seppo_lib.Logr in
  Mirage_crypto_rng_unix.use_default ();
  (match Cg.Request.(from_env () |> consolidate |> proxy) with
   | Error _ -> Sys.argv
                |> Array.to_list
                |> Shell.exec
   | Ok req -> 
     let uuid = Uuidm.v4_gen (Random.State.make_self_init ()) () in
     req
     |> Cgi.handle uuid stdin
     |> Cg.Response.flush uuid stdout)
  |> exit
