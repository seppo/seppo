(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(* https://ohama.github.io/ocaml/ocamllex-tutorial/simpleexample/ *)
{ }

  rule url buf = parse
| "http" 's'? "://" [^' ']+ as lxm {
    let buf =
      let u = lxm |> Uri.of_string in
      let off = match Uri.scheme u with
        | None   -> 0
        | Some s -> 3 + (String.length s) in
      let len = lxm |> String.length in
      let ( |^ ) b s = Buffer.add_string b s; b in
      buf
      |^ "<a href=\""
      |^ (u |> Uri.to_string)
      |^ "\">"
      |^ (String.sub lxm off (len - off))
      |^ "</a>" in
    url buf lexbuf
  }
| '\n' {
  Buffer.add_string buf "<br/>\n";
  url buf lexbuf
}
| _ as c {
  Buffer.add_char buf c;
  url buf lexbuf
}
| eof { buf }

