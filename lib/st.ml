
let camel = "🐫"
let seppo_s = "Seppo.mro.name"
let seppo_u = Uri.make ~scheme:"https" ~host:seppo_s ()
let seppo_c = "Seppo - Personal Social Web"

open Astring

let is_prefix = String.is_prefix
let is_suffix = String.is_suffix

let after ~prefix s : string option =
  if String.is_prefix ~affix:prefix s
  then Some (String.sub ~start:(String.length prefix) s
             |> String.Sub.to_string)
  else None

let before ~suffix s : string option =
  if String.is_suffix ~affix:suffix s
  then Some (String.sub ~stop:((String.length s) - (String.length suffix)) s
             |> String.Sub.to_string)
  else None

let updir path =
  match path
        |> String.cuts ~sep:"/"
        |> List.fold_left (fun (l,i) s ->
            match s,i with
            | ("",0)  -> (l,0)
            | (_,0)   -> (l,1)
            | (".",i) -> (l,i)
            | _       -> (".." :: l,i+1)) ([],0) with
  | [],_ -> ""
  | l,_  -> (l |> String.concat ~sep:"/") ^ "/"

let last l =
  let flast _ v = v in
  List.fold_left flast (List.hd l) l

let is_monotonous cmp l =
  let step (r,p) o =
    if (not r) || cmp p o < 0
    then (false,p)
    else (r,o)
  in
  let r,_ = List.fold_left step (true,List.hd l) l in
  r

let window ?(size = 50) ?(page = 0) l =
  let mi = page * size in
  let ma = mi + size in
  l
  |> List.filteri (fun i _ -> mi <= i && i < ma)

let pp_json_err ppf e = Format.pp_print_string ppf (e |> Ezjsonm.read_error_description)
let pp_exc ppf e = Format.pp_print_string ppf (Printexc.to_string e)
