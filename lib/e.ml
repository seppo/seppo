
let ee = Printf.sprintf "see https://%s/E%04d" St.seppo_s

let e1001 = ee 1001
let e1002 = ee 1002
let e1003 = ee 1003
let e1004 = ee 1004
let e1005 = ee 1005 (* uncaught *)
let e1006 = ee 1006
let e1007 = ee 1007
let e1008 = ee 1008
let e1009 = ee 1000

let e1010 = ee 1010
let e1011 = ee 1011
let e1012 = ee 1012
(* let e1013 = ee 1013 *)
(* let e1014 = ee 1014 *)
let e1015 = ee 1015
let e1016 = ee 1016
let e1017 = ee 1017
let e1018 = ee 1018
let e1019 = ee 1019

let e1020 = ee 1020
let e1021 = ee 1021
let e1022 = ee 1022
let e1023 = ee 1023
let e1024 = ee 1024
let e1025 = ee 1025
let e1026 = ee 1026
let e1027 = ee 1027
let e1028 = ee 1028
let e1029 = ee 1029

let e1030 = ee 1030
let e1031 = ee 1031 (* ln -s ../.well-formed/webfinger/.htaccess ... *)
let e1032 = ee 1032
let e1033 = ee 1033
let e1034 = ee 1034 (* cannot launch *)
let e1035 = ee 1035
let e1036 = ee 1036
let e1037 = ee 1037
let e1038 = ee 1038
let e1039 = ee 1039

let e1040 = ee 1040
let e1041 = ee 1041
let e1042 = ee 1042
let e1043 = ee 1043
let e1044 = ee 1044
let e1045 = ee 1045
let e1046 = ee 1046
let e1047 = ee 1047
let e1048 = ee 1048
let e1049 = ee 1049 (** actor webfinger or profile get *)

let e9001 = ee 9001
