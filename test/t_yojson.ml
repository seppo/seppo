(*
 * https://github.com/ocaml-community/yojson/blob/master/examples/constructing.ml
 *)
let test_constructing () =
  `Assoc
    [
      ("id", `String "398eb027");
      ("name", `String "John Doe");
      ( "pages",
        `Assoc
          [ ("id", `Int 1); ("title", `String "The Art of Flipping Coins") ] );
    ]
  |> Yojson.Basic.pretty_to_string
  |> Assrt.equals_string __LOC__
    "{\n\
    \  \"id\": \"398eb027\",\n\
    \  \"name\": \"John Doe\",\n\
    \  \"pages\": { \"id\": 1, \"title\": \"The Art of Flipping Coins\" }\n\
     }"

let () =
  test_constructing ();
  assert true
