(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * uri_time.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let test_resolve () =
  let a = Uri.of_string "https://example.xom/a/a.a/?a#a"
  and b = Uri.of_string "./b?b#b" in
  Http.reso ~base:a b
  |> Uri.to_string
  |> Assrt.equals_string __LOC__ "https://example.xom/a/a.a/b?b#b";
  let b = Uri.of_string "http://example.com/c?c#c" in
  Http.reso ~base:a b
  |> Uri.to_string
  |> Assrt.equals_string __LOC__ "http://example.com/c?c#c"

let test_00 () =
  let sub ?(pos = 0) ?(len = -1) s =
    let len = if len >= 0
      then len
      else (String.length s) - pos in
    String.sub s pos len
  in
  Uri.make
    ~userinfo:"fo@ o"
    ~host:"example.com"
    ()
  |> Uri.to_string
  |> sub ~pos:2
  |> Assrt.equals_string __LOC__ "fo%40%20o@example.com"

let test_0 () =
  Logr.info (fun m -> m "uri_test.test_0" );
  let ends_with ~suffix s =
    let l = s |> String.length
    and l' = suffix |> String.length in
    let rec f i' i =
      (* Logr.info (fun m -> m "uri_test.test_0 f %d %d" i' i); *)
      let i' = pred i'
      and i  = pred i in
      i' < 0
      || (suffix.[i'] = s.[i]
          && (f i' i))
    in
    (l' <= l)
    && (f l' l)
  in
  assert (ends_with ~suffix:"" "");
  assert (ends_with ~suffix:"s" "s");
  assert (ends_with ~suffix:"/seppo.cgi" "uhu/seppo.cgi");
  assert (ends_with ~suffix:"🐫" "s🐫");
  assert (not (ends_with ~suffix:"s" ""));
  assert (not (ends_with ~suffix:"s" "su"));
  assert (not (ends_with ~suffix:"s" "su"));
  assert true


let test_1 () =
  Logr.info (fun m -> m "uri_test.test_1" );
  let u = "https://uid@example.com/dir/seppo.cgi"
          |> Uri.of_string
  in
  u |> Uri.user |> Option.get |> Assrt.equals_string __LOC__ "uid" ;
  u |> Uri.host |> Option.get |> Assrt.equals_string __LOC__ "example.com" ;
  u |> Uri.path |> Assrt.equals_string __LOC__ "/dir/seppo.cgi" ;
  let u = u |> Uri.with_uri ~userinfo:(Some "ufo") in
  u |> Uri.user |> Option.get |> Assrt.equals_string __LOC__ "ufo" ;
  assert true

let () =
  test_resolve ();
  test_00 ();
  test_0 ();
  test_1 ();
  assert true
