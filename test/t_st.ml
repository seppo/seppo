(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)
open Seppo_lib

let test_dirname () =
  "./foo/../bar/baz" |> Filename.dirname |> Assrt.equals_string __LOC__ "./foo/../bar"

let test_updir () =
  "" |> St.updir |> Assrt.equals_string __LOC__ "";
  "foo" |> St.updir |> Assrt.equals_string __LOC__ "";
  "foo/" |> St.updir |> Assrt.equals_string __LOC__ "../";
  "/foo/" |> St.updir |> Assrt.equals_string __LOC__ "../";
  "foo/bar" |> St.updir |> Assrt.equals_string __LOC__ "../";
  "/foo" |> St.updir |> Assrt.equals_string __LOC__ "";
  "/foo/bar" |> St.updir |> Assrt.equals_string __LOC__ "../";
  "/foo/bar/" |> St.updir |> Assrt.equals_string __LOC__ "../../";
  "/.well-known/webfinger/.htaccess" |> St.updir |> Assrt.equals_string __LOC__ "../../";
  ".well-known/webfinger/.htaccess" |> St.updir |> Assrt.equals_string __LOC__ "../../"

let test_before () =
  "uhu/index.xml"
  |> St.before ~suffix:"index.xml"
  |> Option.get
  |> Assrt.equals_string __LOC__ "uhu/"

let test_window () =
  Logr.info (fun m -> m "%s" "test_window");
  let tos l = l |> List.map Int.to_string |> String.concat "; " in
  let l0 = List.init 37 (fun i -> i) in
  l0 |> tos |> Assrt.equals_string __LOC__ "0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 32; 33; 34; 35; 36";
  l0
  |> St.window ~page:2 ~size:4
  |> tos |> Assrt.equals_string __LOC__ "8; 9; 10; 11"

let test_lambdasoup () =
  let open Soup in
  let soup = parse "<p class='Hello'>World!</p>" in
  wrap (soup $ ".Hello" |> R.child) (create_element "strong");
  soup |> to_string
  |> Assrt.equals_string __LOC__  "<p class=\"Hello\"><strong>World!</strong></p>"

let () =
  Logr.info (fun m -> m "st_test");
  Unix.chdir "../../../test/";
  test_dirname ();
  test_updir ();
  test_before ();
  test_window ();
  test_lambdasoup ();
