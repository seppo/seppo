(entry
  (title "🐫 📺 25 Years of #OCaml: Xavier #@Leroy - Watch OCaml")
  (id ag8gdh2)
  (updated "2022-01-18T08:41:30+01:00")
  (published "2022-01-18T08:37:12+01:00")
  (link "https://watch.ocaml.org/videos/watch/e1ee0fc0-50ef-4a1c-894a-17df181424cb")
  (categories @Leroy AD2021 OCaml PeerTube 🐫 📺)
  (content "\"Professor Xavier Leroy -- the primary original author and leader of the OCaml project -- reflects on 25 years of the OCaml language at his OCaml Workshop #AD2021 keynote speech.\" #PeerTube

via https://discuss.ocaml.org/t/ann-first-announcement-of-bechamel/9164/2?u=mro")
)
(entry
  (title "🖥 Mit der richtigen #Linux -Distribution zum Erfolg | heise online")
  (id ag8gb4e)
  (updated "2022-01-18T08:32:47+01:00")
  (published "2022-01-18T08:13:00+01:00")
  (link "https://heise.de/-6320516")
  (categories Linux 🖥)
  (content "\"… Bei den Linux-Varianten legt auch keine Marketingabteilung fest, dass Ihr Computer plötzlich aufs Abstellgleis gehört. …\" Plan: https://www.heise.de/select/ct/2022/3/2132112064640999627

Mir fehlen einige, über die ich selber gerne mehr wüßte, z.B.

- voidlinux.org
- parabola.nu
- lubuntu.me & xubuntu.org
- slitaz.org")
)
(entry
  (title "#§ Verschlusssache Wiederbewaffnung | Telepolis")
  (id ag8cey7)
  (updated "2022-01-17T17:31:57+01:00")
  (published "2022-01-17T17:28:05+01:00")
  (link "https://heise.de/-6329123")
  (categories @Roth @Weber Grüne Kanzleramt NATO §)
  (content "\"… Zu einer davon abweichenden und der Einschätzung von Bundesarchiv und Bundeskanzleramt zuwiderlaufenden Sachentscheidung besteht – insbesondere mit Blick auf eine mögliche Gefährdung des Staatswohls – keine Veranlassung. …\"

Keine Macht für Niemand, gell Frau #@Roth. #Grüne #@Weber #NATO #Kanzleramt 

via https://blog.fefe.de/?ts=9f1bb8c3")
)
(entry
  (title "🔭 💫 This Is No Way to Be Human - The Atlantic")
  (id ag879kx)
  (updated "2022-01-16T21:27:09+01:00")
  (published "2022-01-16T21:25:57+01:00")
  (link "https://www.theatlantic.com/technology/archive/2022/01/machine-garden-natureless-world/621268")
  (categories Astronomie Computer Teleskop 💫 🔭)
  (content "\"… These days, professional astronomers rarely look at the sky through the lens of a telescope. They sit at #computer screens. …\"
#Astronomie #Teleskop")
)
(entry
  (title "🐽§👮 #Verfassungsschutz -Chef: Virtuelle Agenten operieren erfolgreich auf #Telegram | heise online")
  (id ag877k7)
  (updated "2022-01-16T21:08:51+01:00")
  (published "2022-01-16T21:06:29+01:00")
  (link "https://heise.de/-6328347")
  (categories BfV Telegram Verfassungsschutz 🐽§👮)
  (content "\"… bei Hass und Hetze auf dem Messenger-Dienst Telegram alles so weit im Griff. …\" #BfV")
)
(entry
  (title "🇩🇪 Ampel: Geheimsache digitale Meldeketten | heise online")
  (id ag7xfh9)
  (updated "2022-01-15T15:42:14+01:00")
  (published "2022-01-15T15:39:43+01:00")
  (link "https://heise.de/-6328176")
  (categories Bundesregierung Bundestag Digitalisierung RKI 🇩🇪)
  (content "\"… Den Bericht habe sie mündlich erstattet.
…
Im Übrigen sagt Lauterbachs Ministerium die Unwahrheit: Eine Vorschrift, die die Vertraulichkeit von Äußerungen in allen Bundestagsausschüssen vorschreiben würde, gibt es gar nicht. …\"
#Bundesregierung #Bundestag #Digitalisierung #RKI")
)
(entry
  (title "Umstrittener Farbstoff: Europäische Kommission 🇪🇺 verbietet Titandioxid in Lebensmitteln - DER SPIEGEL")
  (id ag7kkae)
  (updated "2022-01-14T20:56:41+01:00")
  (published "2022-01-14T20:53:48+01:00")
  (link "https://spiegel.de/purl-a-b0b7cfc6-b5da-4935-97cc-d7c49f015146")
  (categories AD1908 Efsa 🇪🇺)
  (content "seit #AD1908 E171 TiO₂ #Efsa")
)
(entry
  (title "🖨️ #Drucker -zubehör für #Brother #HL-2030 in Original und Alternativ günstig bei ASC kaufen")
  (id ag7g7b8)
  (updated "2022-01-14T11:31:20+01:00")
  (published "2022-01-14T11:27:42+01:00")
  (link "https://www.asc-toner.net/Brother/HL-2030-Toner-Trommeln-Druckerzubehoer")
  (categories Brother Drucker HL-2030 Hardware 🖨)
  (content "#Hardware, auch https://geizhals.de/kompatible-trommel-zu-brother-dr-2000-a248027.html")
)
(entry
  (title "🐽§👮 🌧 🇩🇪 \"Wo ein Trog ist, sammeln sich die Schweine\" | Telepolis")
  (id ag7c47b)
  (updated "2022-01-13T19:39:43+01:00")
  (published "2022-01-13T19:35:45+01:00")
  (link "https://heise.de/-2570570")
  (categories AD2009 BVerfG Bundesregierung Datensparsamkeit Vorratsdatenspeicherung 🇩🇪 🌧 🐽§👮)
  (content "Christoph Gusy #AD2009 am #BVerfG Karlsruhe anläßlich #Vorratsdatenspeicherung auf Wunsch der #Bundesregierung. #Datensparsamkeit")
)
(entry
  (title "#† #RIP #AD2022 🎬 🇩🇪 Herbert Achternbusch")
  (id ag7bshz)
  (updated "2022-01-13T18:24:32+01:00")
  (published "2022-01-13T18:23:11+01:00")
  (link "https://sz.de/1.5505860")
  (categories AD2022 RIP † 🇩🇪 🎬)
  (content "")
)
(entry
  (title "🐽§👮 Villarejo claims that Spain's 🇪🇸 intel was involved in the #Barcelona terrorist attack of #ad2017 - VilaWeb")
  (id ag79e49)
  (updated "2022-01-13T09:46:05+01:00")
  (published "2022-01-13T09:39:19+01:00")
  (link "https://english.vilaweb.cat/noticies/villarejo-claims-that-spains-intel-was-involved-in-the-barcelona-terrorist-attack-of-2017")
  (categories AD2017 Barcelona Geheimdienst Terror 🇪🇸 🐽§👮)
  (content "Die haben auch Leute angeheuert, die Dinge tun wollen, für die man sonst in den Knast geht. #Geheimdienst #Terror 
via https://blog.fefe.de/?ts=9f202d31")
)
(entry
  (title "📻 Das #Kinder - und Familienradio | #Radio TEDDY")
  (id ag79bd9)
  (updated "2022-01-13T09:14:38+01:00")
  (published "2022-01-13T09:14:07+01:00")
  (link "https://www.radioteddy.de")
  (categories Kinder Radio 📻)
  (content "https://irmedia.streamabc.net/irm-rtlive-mp3-128-6306268?sABC=61qsqr7q%230%239s2rs985s3985o4306q0899o64sqno57%23enqvbcynlre_ncc_zc3&___cb=477474767929672&amsparams=playerid:radioplayer_app_mp3;skey:1642061437")
)
(entry
  (title "#† #RIP #AD2022 🇩🇪 Ali Mitgutsch, Wimmelbuch-Autor 🎨")
  (id ag6y36u)
  (updated "2022-01-11T21:23:27+01:00")
  (published "2022-01-11T21:21:08+01:00")
  (link "https://www.br.de/nachrichten/kultur/wimmelbuch-autor-ali-mitgutsch-gestorben,SuEZyw5")
  (categories AD2022 RIP † 🇩🇪 🎨)
  (content "")
)
(entry
  (title "The #Internet is for End Users | #RFC 8890 #AD2020")
  (id ag6y2b4)
  (updated "2022-01-11T21:16:03+01:00")
  (published "2022-01-11T21:13:14+01:00")
  (link "https://datatracker.ietf.org/doc/html/rfc8890")
  (categories AD2020 Design Internet RFC Seppo Web)
  (content "Humans in fact! urn:ietf:rfc:8890 #Design #Web #Seppo
via https://news.ycombinator.com/item?id=29894998#29897124")
)
(entry
  (title "🌍 Hundredrabbits")
  (id ag6bs55)
  (updated "2022-01-09T22:17:29+01:00")
  (published "2022-01-09T22:08:27+01:00")
  (link "http://100r.co")
  (categories Art Design sustainable 🌍)
  (content "#sustainable low tech on a boat. #Design #Art
via https://news.ycombinator.com/item?id=29859059")
)
(entry
  (title "MumbleLink - Mods - #Minecraft - CurseForge")
  (id ag65yg3)
  (updated "2022-01-08T23:49:19+01:00")
  (published "2022-01-08T23:48:49+01:00")
  (link "https://www.curseforge.com/minecraft/mc-mods/mumblelink/files")
  (categories Minecraft 📺)
  (content "via 📺 https://www.youtube-nocookie.com/embed/Bi6gnu3GHWQ?")
)
(entry
  (title "📻 ✇ ♫ Der Name der Rose - #Hörspiel nach Umberto #@Eco #AD1986 · ARD Audiothek")
  (id ag642xg)
  (updated "2022-01-08T16:34:17+01:00")
  (published "2022-01-08T16:30:14+01:00")
  (link "https://www.ardaudiothek.de/sendung/der-name-der-rose-hoerspiel-nach-umberto-eco/95818886")
  (categories @Eco AD1986 Podcast Hörspiel ♫ ✇ 📻)
  (content "leider kein #Podcast.
via http://rec.mro.name/stations/b2/2022/01/08/1505.xml")
)
(entry
  (title "📱 🍏 Snapshot testing in #iOS - Speaker Deck | #Talk by Bartłomiej Hyży, flightradar24, #AD2019")
  (id ag5h56x)
  (updated "2022-01-06T22:42:22+01:00")
  (published "2022-01-06T22:37:57+01:00")
  (link "https://speakerdeck.com/bhyzy/snapshot-testing-in-ios")
  (categories AD2019 Apple Software Talk Test dev iOS unit 🍏 📱)
  (content "#Apple, #dev, #Software, #Test, #Unit")
)
(entry
  (title "💸 🐍 📈 downloading transactions from FinTS banking APIs and sorting them into a ledger journal | GitHub - MoritzR/fints2ledger")
  (id ag5fxau)
  (updated "2022-01-06T17:54:19+01:00")
  (published "2022-01-06T17:51:32+01:00")
  (link "https://github.com/MoritzR/fints2ledger")
  (categories API Bank GLS Python hledger 🐍 💸 📈)
  (content "FinTS/HBCI #API -> #hledger via #Python auch für die #GLS #Bank.")
)
(entry
  (title "🚗 ⚡️ Kommentar: Das Bullshit-Bingo der Verkehrswende | heise online")
  (id ag5fabd)
  (updated "2022-01-06T15:47:48+01:00")
  (published "2022-01-06T15:46:59+01:00")
  (link "https://heise.de/-6316848")
  (categories ⚡ 🚗)
  (content "\"… Ein paar Schlüsselbegriffe, die bei jeder Debatte früher oder später auftauchen – und was dahinter steckt. …\"")
)
(entry
  (title "🤣 Emojiquette")
  (id ag593c7)
  (updated "2022-01-05T15:39:32+01:00")
  (published "2022-01-05T15:37:41+01:00")
  (link "https://github.com/Schneegans/Burn-My-Windows#octocat-i-want-to-contribute")
  (categories 🤣)
  (content "via https://news.ycombinator.com/item?id=29795508&p=2#29796713")
)
(entry
  (title "🌍 📺 📈 Arithmetic, Population and Energy - a talk by Al Bartlett - YouTube")
  (id ag57898)
  (updated "2022-01-05T15:44:30+01:00")
  (published "2022-01-05T08:43:42+01:00")
  (link "https://www.youtube-nocookie.com/embed/O133ppiVnWY?")
  (categories Arithmetik Math ∃ ∠ 🌍 📈 📺)
  (content "\"… The Greatest Shortcoming of the Human Race is our Inability to understand the exponential Function …\"

70 / Prozentwachstum = Verdopplungszeit

Z.B. 70 / 7 % jährliches Wachstum = Verdopplung in 10 Jahren

(Bakterien ab 22\", Flächendarstellung danach)
via https://blog.fefe.de/?ts=9f2db2a6 #Math #Arithmetik ∃ ∠")
)
(entry
  (title "😷 Die Begegnung und der öffentliche Raum werden zu Unrecht verteufelt")
  (id ag575gb)
  (updated "2022-01-05T08:28:10+01:00")
  (published "2022-01-05T08:17:45+01:00")
  (link "https://www.nachdenkseiten.de/?p=79448")
  (categories Demo Kinder 💉 😷)
  (content "\"… Die Beschränkungen von Weihnachtsmärkten und Silvesterfeiern sowie der Teilnehmerzahl von Außenveranstaltungen aller Art sind nach wissenschaftlicher Literaturlage eigentlich nicht zu rechtfertigen. Dies gilt selbstverständlich auch für Demonstrationen.
…
Quarantänepflichten – insbesondere für Kinder und Jugendliche, aber auch für Menschen ohne Symptome – sollten schnellstmöglich überdacht werden. Es gibt auch keinerlei wissenschaftliche Evidenz, dass hierbei zwischen Geimpften und Ungeimpften unterschieden werden sollte. …\" 💉 #Demo #Kinder")
)
(entry
  (title "💸 ⚡️ Die teuerste Haftpflichtpolice der Welt #AD2011 | Fefes Blog")
  (id ag4z827)
  (updated "2022-01-04T09:43:19+01:00")
  (published "2022-01-04T09:38:29+01:00")
  (link "https://www.manager-magazin.de/finanzen/versicherungen/a-761954.html")
  (categories Energie Müll AD2011 Waffen ⚡ 💸)
  (content "\"Finanzmathematiker haben erstmals errechnet, wie teuer eine Haftpflichtpolice für ein Atomkraftwerk wäre - 72 Milliarden Euro jährlich.\"

Es ging nie um #Energie, es ging immer um #Waffen. Deswegen übernimmt der Staat \"großzügig\" den #Müll und die Risiken. via https://blog.fefe.de/?ts=9f2db2a6")
)
(entry
  (title "Minetest – Block für Block zur kantigen Traumwelt - Pro-Linux")
  (id ag4tcz8)
  (updated "2022-01-03T19:05:10+01:00")
  (published "2022-01-03T19:04:30+01:00")
  (link "https://www.pro-linux.de/artikel/2/1741/minetest-block-f%C3%BCr-block-zur-kantigen-traumwelt.html")
  (categories Minecraft)
  (content "wie kommen Tiere in den #Minecraft Konkurrenten?")
)
(entry
  (title "🖌️ 🎨 Twilight Edge #Software - PikoPixel for Mac/Linux/BSD")
  (id ag4kxc7)
  (updated "2022-01-03T09:34:58+01:00")
  (published "2022-01-03T09:13:41+01:00")
  (link "http://twilightedge.com/mac/pikopixel/index.html")
  (categories Linux Minecraft Software macOS skin 🎨 🖌)
  (content "#macOS & #Linux Malprogramm, super z.B. für #Minecraft #skin
https://packages.debian.org/de/buster/pikopixel.app
Templates: https://minecraft.fandom.com/wiki/Skin#Templates

via https://conceptartempire.com/pixel-art-software/")
)
(entry
  (title "🥌 Sebastian #@Jacoby über #Curling: »Die Leute sagen, das sei so ein Hausfrauending« - DER SPIEGEL")
  (id ag489ut)
  (updated "2022-01-01T16:40:29+01:00")
  (published "2022-01-01T16:39:31+01:00")
  (link "https://spiegel.de/purl-a-91d4be5d-1b20-4423-9833-2ad6c76df942")
  (categories @Jacoby Curling 🥌)
  (content "\"… Sebastian Jacoby ist als »Quizgott« aus der ARD bekannt. Was viele nicht wissen: Er war auch Curling-Europameister. Hier erklärt er, warum der Sport zu Unrecht belächelt wird – und wie sich das ändern ließe. …\"")
)
(entry
  (title "🏔 BergWegVerlag")
  (id ag474by)
  (updated "2022-01-01T12:20:21+01:00")
  (published "2022-01-01T11:56:46+01:00")
  (link "https://www.bergwegverlag.de")
  (categories @Heumader Allgäu sustainable 🏔 📖 📻)
  (content "Bücher und Bildbände über \"Stadel und Schinde\" urn:isbn:978-3-00-041703-0 oder
\"Milch\" urn:isbn:978-3-00-066273-7
Christian #@Heumader via 📻 http://rec.mro.name/stations/b2/2022/01/01/1105.xml
#sustainable #Allgäu")
)
(entry
  (title "🎬 🇯🇵 Moderner Magier: Zeichentricklegende Hayao #@Miyazaki | BR KulturBühne | BR.de")
  (id ag46s3u)
  (updated "2022-01-01T10:50:30+01:00")
  (published "2022-01-01T10:36:44+01:00")
  (link "https://www.br.de/kultur/studio-ghibli-animator-hayao-mayazaki-80-geburtstag-100.html")
  (categories @Miyazaki 🇯🇵 🎬)
  (content "")
)
(entry
  (title "📺 🚂 🚄 Stuttgart 21 - Die ganze Wahrheit! Die #Anstalt vom 29.01.2019 | ZDF - YouTube")
  (id ag3tb55)
  (updated "2021-12-30T22:39:12+01:00")
  (published "2021-12-30T22:37:15+01:00")
  (link "https://www.youtube-nocookie.com/embed/V49b13fYFik?")
  (categories AD2019 Anstalt S21 📺 🚂 🚄)
  (content "#S21 #AD2019 auch https://www.youtube.com/watch?v=Luwlvs1YiA8")
)
(entry
  (title "S-Expressions | an I-D by Ron Rivest #AD1997")
  (id ag3keeh)
  (updated "2021-12-30T11:48:48+01:00")
  (published "2021-12-30T11:38:39+01:00")
  (link "http://people.csail.mit.edu/rivest/Sexp.txt")
  (categories AD1997 RFC)
  (content "almost a #RFC.")
)
(entry
  (title "Switch from #XML to S-Expressions file format · Issue _#192 · LibrePCB/LibrePCB · GitHub")
  (id ag3ke7f)
  (updated "2021-12-30T11:36:26+01:00")
  (published "2021-12-30T11:35:49+01:00")
  (link "https://github.com/LibrePCB/LibrePCB/issues/192")
  (categories XML)
  (content "")
)
(entry
  (title "♫ Auf de schwäb'sche Eisebahne - #Noten, Liedtext, MIDI, Akkorde")
  (id ag3byk3)
  (updated "2021-12-29T10:24:00+01:00")
  (published "2021-12-29T10:23:13+01:00")
  (link "https://www.lieder-archiv.de/auf_de_schwaebsche_eisebahne-notenblatt_400120.html")
  (categories Noten ♫)
  (content "oder https://liederprojekt.org/lied30298-Auf_de_schwaebsche_Eisebahne.html")
)
(entry
  (title "📧 How to send an #email – Aral #@Balkan")
  (id ag38tzk)
  (updated "2021-12-28T22:26:51+01:00")
  (published "2021-12-28T22:26:16+01:00")
  (link "https://ar.al/2021/12/20/how-to-send-an-email")
  (categories @Balkan Email 📧)
  (content "")
)
(entry
  (title "Manually booting the #Linux kernel from #GRUB")
  (id ag38gba)
  (updated "2021-12-28T21:40:30+01:00")
  (published "2021-12-28T21:32:32+01:00")
  (link "https://www.unix-ninja.com/p/Manually_booting_the_Linux_kernel_from_GRUB")
  (categories Linux grub)
  (content "\"…und dann
$ sudo grub-install /dev/sda # https://wiki.ubuntuusers.de/GRUB_2/Installation/")
)
(entry
  (title "💸 Zimt - Naturwelt/Menschenrechte/ Gudrun Kaspareit/ Standing Rock/ Protest gegen die Dakota Access Pipeline")
  (id ag24edx)
  (updated "2021-12-24T10:13:29+01:00")
  (published "2021-12-24T09:43:09+01:00")
  (link "https://www.naturwelt.org/kr%C3%A4uter/gew%C3%BCrze/zimt")
  (categories AD1500 Augsburg Fugger 💸 📺)
  (content "\"… Anton #Fugger (Kaufmann und Bankier um #AD1500 in #Augsburg) verbrannte als Zeichen seines übergroßen Reichtums einen Schuldschein Kaiser Karls V. in einem Zimtfeuer. …\"

via 📺 http://rec.mro.name/stations/b2/2021/12/24/0905.xml")
)
(entry
  (title "#§ 🇩🇪 Recht auf \"schnelles\" #Internet: Mindestens 10 MBit/s sollen es sein | heise online")
  (id afzu37a)
  (updated "2021-12-23T09:08:04+01:00")
  (published "2021-12-23T08:52:32+01:00")
  (link "https://heise.de/-6305666")
  (categories BNetzA Bundesnetzagentur Bundestag Internet TKG § 🇩🇪)
  (content "56 KBit/s #TKG aktuell
10 MBit/s #BNetzA 
30 MBit/s #Bundestag
50 MBit/s Verbraucherzentralen

auch https://glm.io/161985 und https://news.ycombinator.com/item?id=29659783")
)
(entry
  (title "😷 heute Morgen auf SPON so: Lauterbach 🎀 will schnelle Impfflicht 💉 …")
  (id afzhe26)
  (updated "2021-12-22T15:41:57+01:00")
  (published "2021-12-22T15:24:04+01:00")
  (link "")
  (categories 🎀 💉 😷)
  (content "… aber nicht wegen Omikron, dagegen bringe das nichts mehr, sondern gegen zukünftige Dingens.

Wieso dann grad jetzt, ist die Stimmung grad passend?

Inzwischen ist der Artikel wieder weg.")
)
(entry
  (title "🐔 🍲 👨‍🍳 🍳 Erkältungssüppchen vom Huhn von Kräuterjule | Chefkoch")
  (id afzg9ah)
  (updated "2021-12-22T10:50:57+01:00")
  (published "2021-12-22T10:49:03+01:00")
  (link "https://www.chefkoch.de/rezepte/1184801224492115/Erkaeltungssueppchen-vom-Huhn.html")
  (categories Rezept 🍲 🍳 🐔 👨‍🍳)
  (content "#Rezept")
)
(entry
  (title "🌳 Kontakt zur Natur 🌿 hilft gegen Einsamkeit - DER SPIEGEL")
  (id afzby3z)
  (updated "2021-12-21T18:02:28+01:00")
  (published "2021-12-21T17:58:23+01:00")
  (link "https://spiegel.de/purl-a-cffa422d-4198-4d4f-89a9-129a66c770da")
  (categories 🌳 🌿)
  (content "\"… Wer sich einsam fühlt, lebt ungesünder. Das empfundene Fehlen einer Verbindung zu anderen Menschen ist eines der stärksten Anzeichen für spätere Gesundheitsprobleme – Depressionen, Alkoholsucht, Demenz, aber auch Immun- oder Herzkreislauferkrankungen. Laut einer Studie, die am Montag in der Zeitschrift »Scientific Reports« erschien, erhöht Einsamkeit das Sterberisiko um 45 Prozent, mehr als Luftverschmutzung oder Übergewicht.

Das Autorenteam von Psychologen am Londoner King's College hat Faktoren erforscht, die zu diesem Gefühl beitragen oder dagegen helfen. Große Menschenmengen in Städten verstärken die empfundene Einsamkeit deutlich. Werden die Mitmenschen als wohlwollend wahrgenommen, sinkt sie hingegen. Noch mehr aber hilft Kontakt mit der Natur. Befragte, die Pflanzen, den Himmel, Wasser sehen oder Vögel hören konnten, fühlten sich 28 Prozent weniger einsam als die anderen Teilnehmer der Studie. …\"")
)
(entry
  (title "♫ LNP416 Zusammenrottung aus dem Kreise der politischen Gegner mit Hacking-Affinität | Logbuch:Netzpolitik")
  (id afz9f28)
  (updated "2021-12-21T09:07:08+01:00")
  (published "2021-12-21T08:50:30+01:00")
  (link "https://logbuch-netzpolitik.de/lnp416-zusammenrottung-aus-dem-kreise-der-politischen-gegner-mit-hacking-affinitaet#comment-171201")
  (categories @Linus @Pritlove @Schäuble Glyphosat IndieWeb Klarnamenpflicht POSSE Seppo iOS ♫ 🐛 🐞 📱)
  (content "#@Pritlove & #@Linus Neumann über log4j, #Klarnamenpflicht, Pegasus/NSO, #iOS 🐛 🐞 📱. Kommentar von mir: 

Klarnamenpflicht?

Gibt’s ja in ‚echt‘ quasi nie – z.B. nicht wenn ich über die Straße gehe, nie wenn ich mich nicht ausweise. Außer ich habe den Perso an der Brust. Das wäre Klarnamenpflicht.

Wieso also (nur) im Netz? Weil die, die es wollen, glauben, davon nicht betroffen zu werden. #@Schäuble meinte im Sommer, es gäbe im Netz „… Schweinereien, die es so „in der realen, analogen Welt“ nicht gebe. …“. An Geldkoffer, Ruanda, Srebrenica, Auschwitz, Kambodscha, Kindesmißbrauch und #Glyphosat, denkt er & Co. nicht.

Eine Lösung? Publizieren auf der eigenen Domain. #IndieWeb / #Posse. Disclaimer: https://seppo.app #Seppo")
)
(entry
  (title "🐺 Ein #Wolf geht um in #Bergen - #Bayern - SZ.de")
  (id afz777g)
  (updated "2021-12-20T23:56:47+01:00")
  (published "2021-12-20T23:55:02+01:00")
  (link "https://sz.de/1.5490622")
  (categories Bayern Bergen Wolf 🐺)
  (content "")
)
(entry
  (title "💸 📈 #hledger system for chaotic #German 🇩🇪 freelancers based on the #SKR04 chart of accounts by #DATEV bearing in mind the GoBD principles")
  (id afz422u)
  (updated "2021-12-20T11:38:09+01:00")
  (published "2021-12-20T11:33:56+01:00")
  (link "https://github.com/rotorkunstkultur/rtrledger")
  (categories Buchhaltung German SKR04 datev hledger 🇩🇪 💸 📈)
  (content "#Buchhaltung und AfA! https://github.com/rotorkunstkultur/DepreciateForLedger")
)
(entry
  (title "📺 The folding process of #origami Samurai Warrior - YouTube")
  (id afyk9a9)
  (updated "2021-12-18T22:20:40+01:00")
  (published "2021-12-18T22:20:07+01:00")
  (link "https://www.youtube-nocookie.com/embed/MtIf5WHTbns?wmode=transparent?")
  (categories Origami 🇯🇵 📺)
  (content "via https://news.ycombinator.com/item?id=29604560")
)
(entry
  (title "⚡️ 🌻 🌍 How to Build a Low-tech #Solar Panel? | LOW←TECH MAGAZINE")
  (id afyhx45)
  (updated "2021-12-18T20:44:45+01:00")
  (published "2021-12-18T20:41:39+01:00")
  (link "https://solar.lowtechmagazine.com/2021/10/how-to-build-a-low-tech-solar-panel.html")
  (categories Solar Solarzelle sustainable ⚡ 🌍 🌻)
  (content "\"… For example, a clothesline and a solar thermal water boiler are much more efficient, #sustainable, and economical than an electric tumble dryer and a water boiler powered by solar PV panels. …\" #Solarzelle")
)
(entry
  (title "⭐ 🐛 🐞 📱 🍏 Leider geil: NSOs Pegasus-Exploit für #iPhone -Spyware enthüllt | heise online")
  (id afyc6fs)
  (updated "2021-12-17T22:54:08+01:00")
  (published "2021-12-17T22:51:06+01:00")
  (link "https://heise.de/-6297893")
  (categories BKA Security iPhone ⭐ 🍏 🐛 🐞 📱)
  (content "\"Ausgangspunkt war ein simpler Integer Overflow in einem Kompressionsverfahren namens JBIG2 aus Fax-Zeiten.
…
Da hat jemand Geniales geleistet, damit Diktatoren und das #BKA andere Menschen ausspionieren können. Was für eine Schande ...\"
#Security")
)
(entry
  (title "#BR und SWR: Öffentlich-rechtliche Sender gründen Softwarefirma - Golem.de")
  (id afyc66c)
  (updated "2021-12-17T22:47:40+01:00")
  (published "2021-12-17T22:47:22+01:00")
  (link "https://glm.io/161876")
  (categories BR)
  (content "")
)
(entry
  (title "🌍 How to stop data centres from gobbling up the world’s electricity ⚡️ | Nature")
  (id afy36ag)
  (updated "2021-12-16T12:21:24+01:00")
  (published "2021-12-16T12:15:26+01:00")
  (link "https://www.nature.com/articles/d41586-018-06610-y")
  (categories Bitcoin Energie GAFAM Internet sustainable ⚡ 🌍)
  (content "via https://theconversation.com/the-internet-consumes-extraordinary-amounts-of-energy-heres-how-we-can-make-it-more-sustainable-160639
#GAFAM #Bitcoin #Internet #Energie #sustainable")
)
(entry
  (title "Paper #Website: Start a tiny website from your #notebook")
  (id afxh4f9)
  (updated "2021-12-14T21:34:55+01:00")
  (published "2021-12-14T21:34:07+01:00")
  (link "https://paperwebsite.com")
  (categories Notebook Website)
  (content "via https://news.ycombinator.com/item?id=29550812")
)
(entry
  (title "📱 🍏 🤖 BirdNET im #App #Store")
  (id afxfsxt)
  (updated "2021-12-14T16:59:48+01:00")
  (published "2021-12-14T16:30:19+01:00")
  (link "https://apps.apple.com/de/app/birdnet/id1541842885")
  (categories App Ornithologie Store 🍏 📱 📺 🤖)
  (content "📺 https://www.youtube-nocookie.com/embed/f144CSEoYuk?
via https://birdnet.cornell.edu/ #Ornithologie
via https://news.ycombinator.com/item?id=29539248")
)

