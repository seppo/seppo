
open Lib

type entry = { 
  name    : string; 
  country : string option; 
  email   : string option
}
(* [@@deriving sexp] *)

(* https://gitlab.inria.fr/bmontagu/sexp_decode 
   via https://discuss.ocaml.org/t/combinator-library-for-extracting-data-for-s-exps/10153/5 *)
let test_sexp_a () =
  let open Sexp_decode in
  Logr.info (fun m -> m "test_sexp_a");
  let entry_decoder : entry decoder =
    field "entry"
    @@ let* name = field "name" atom in
    let* country = maybe @@ field "country" atom in
    let+ email = maybe @@ field "email" atom in
    { name; country; email } in
  let address_book_decoder (* : address_book decoder *) = list entry_decoder in
  let _r = run address_book_decoder (Atom "a") in
  Assrt.equals_string __LOC__ "a" "a";
  assert true

module D = Decoders_ezjsonm.Decode

(* https://discuss.ocaml.org/t/combinator-library-for-extracting-data-for-s-exps/10153 *)
let test_decoders () =
  let open D in
  let _announce : entry decoder =
    let* _ = field "type" string
    and* name = field "actor" string in
    succeed ({name; country = None; email = None;})
  in
  let r = (D.decode_string (_announce ) "((type "") (actor uhu))")
          |> Result.get_ok in
  r.name
  |> Assrt.equals_string __LOC__ "u"

let () =
  Unix.chdir "../../../test/";
  test_sexp_a ();
  test_decoders ();
  assert true

