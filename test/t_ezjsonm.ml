
let test_error () =
  let j = {|not valid json|} |> Ezjsonm.from_string_result in
  (match j with
   | Error (`Error (((1,1),(1,3)),`Illegal_literal l)) ->
     l |> Assrt.equals_string __LOC__ "not"
   | _ -> failwith __LOC__);
  (match j with
   | Error e ->
     e
     |> Ezjsonm.read_error_description
     |> Assrt.equals_string __LOC__ {|illegal literal (not)|}
   | _ -> failwith __LOC__);
  ()

let test_constructing () =
  `O
    [
      ("id", `String "398eb027");
      ("name", `String "John Doe");
      ( "pages",
        `O
          [
            ("id", Ezjsonm.int 1); ("title", `String "The Art of Flipping Coins");
          ] );
    ]
  |> Ezjsonm.to_string ~minify:true
  |> Assrt.equals_string __LOC__
    "{\"id\":\"398eb027\",\"name\":\"John \
     Doe\",\"pages\":{\"id\":1,\"title\":\"The Art of Flipping Coins\"}}"

let test_load_peertube () =
  let extract3tries k0 k1 j =
    match Ezjsonm.find j [ k0 ] with
    | `String s -> Some s
    | `A (`String s :: _) -> Some s
    | `A ((`O _ as hd) :: _) -> (
        (* ignore 'type' *)
        match Ezjsonm.find hd [ k1 ] with `String s -> Some s | _ -> None)
    | _ -> None
  in
  let ic = open_in "data/peertube-video.json" in
  ic |> Ezjsonm.from_channel
  |> extract3tries "attributedTo" "id"
  |> Option.get |> Uri.of_string |> Uri.to_string
  |> Assrt.equals_string __LOC__
    "https://tube.network.europa.eu/accounts/edps";
  close_in ic;
  let ic = open_in "data/ap/actor/peertube.0.json" in
  let _a = Ezjsonm.from_channel ic in
  close_in ic;
  assert true

let () =
  Unix.chdir "../../../test/";
  test_error ();
  test_constructing ();
  test_load_peertube ();
  assert true
