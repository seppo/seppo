
let o i = i |> Optint.of_int

let oi i = i |> Optint.of_unsigned_int32

let oo i = i |> Optint.of_int64

let b = Bytes.of_string

let test_ds_mini () =
  let cdb = "mini.cdb" |> Mcdb__.Ds_cdb.open_cdb_in in
  assert (b "Ä" = ("a" |> b |> Mcdb__.Ds_cdb.find_first cdb |> Option.get));
  assert (b "B" = ("b" |> b |> Mcdb__.Ds_cdb.find_first cdb |> Option.get));
  assert (b "ß" = ("s" |> b |> Mcdb__.Ds_cdb.find_first cdb |> Option.get));
  Mcdb__.Ds_cdb.close_cdb_in cdb

(*
let test_pipe_big () =
  let fd0 = Unix.openfile "mini.cdb" [ Unix.O_RDONLY ] 0
  and fd1 =
    Unix.openfile "dst.cdb"
      [ Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC; Unix.O_EXCL ]
      0
  in
  let ctx = Cdb.start fd1 in
  let fkt ctx pair =
    let _ = pair |> Cdb.add fd1 ctx in
    true
  in
  let _ = Cdb.dump fd0 fkt in
  let _ = Cdb.finish fd1 in
  assert true;
  Unix.close fd1;
  Unix.close fd0
*)

let test_ds_hash () =
  assert (0x2b5c4l = ("a" |> b |> Mcdb__.Ds_cdb.hash));
  let k0 =
    "http://www.traunsteiner-tagblatt.de/region+lokal/landkreis-traunstein/traunstein/pressemitteilungen-der-stadt-traunstein_artikel,-Traunstein-20-%E2%80%93-Neue-Medien-im-Mittelpunkt-_arid,198374.html"
  in
  assert (0xc7410a37l = (k0 |> b |> Mcdb__.Ds_cdb.hash));
  Assrt.equals_optint __LOC__ (oo 0xc7410a37L) (oi 0xc7410a37l);
  Assrt.equals_optint __LOC__ (oo 0xc7410a37L) (k0 |> b |> Mcdb__.Ds_cdb.hash |> Optint.of_unsigned_int32)


let test_hash () =
  (* Printf.printf "0x%x\n" ("a" |> b |> Cdb.hash); *)
  Assrt.equals_optint __LOC__ (o 0x2b5c4) ("a" |> b |> Mcdb.hash32_byt);
  let k0 =
    "http://www.traunsteiner-tagblatt.de/region+lokal/landkreis-traunstein/traunstein/pressemitteilungen-der-stadt-traunstein_artikel,-Traunstein-20-%E2%80%93-Neue-Medien-im-Mittelpunkt-_arid,198374.html"
  in
  (* Printf.printf "%x\n" (k0 |> b |> Cdb.hash); *)
  Assrt.equals_optint __LOC__ (oo 0xc7410a37L) (k0 |> b |> Mcdb.hash32_byt)

let _test_ds_big () =
  let k0 =
    "http://www.traunsteiner-tagblatt.de/region+lokal/landkreis-traunstein/traunstein/pressemitteilungen-der-stadt-traunstein_artikel,-Traunstein-20-%E2%80%93-Neue-Medien-im-Mittelpunkt-_arid,198374.html"
  in
  let cdb = "big.cdb" |> Mcdb__.Ds_cdb.open_cdb_in in
  (* Printf.printf "%d\n" (k0 |> b |> Mcdb__.Ds_cdb.find cdb |> Bytes.length); *)
  assert (1000 = (k0 |> b |> Mcdb__.Ds_cdb.find_first cdb |> Option.get |> Bytes.length));
  Mcdb__.Ds_cdb.close_cdb_in cdb

let () =
  Unix.chdir "../../../test/";
  test_ds_mini ();
  test_ds_hash ();
  test_hash ();
  (* test_ds_big (); *)
  assert true
