(* *)
open Seppo_lib

let test_ubase () =
  let nfc = "V\197\169 Ng\225\187\141c Phan"
  and nfd = "Vu\204\131 Ngo\204\163c Phan" in
  Assrt.equals_string __LOC__ "Vũ Ngọc Phan" nfc;
  Assrt.equals_string __LOC__ "Vũ Ngọc Phan" nfd;
  Assrt.equals_string __LOC__ "Vu Ngoc Phan" (Ubase.from_utf8 nfc);
  Assrt.equals_string __LOC__ "Vu Ngoc Phan" (Ubase.from_utf8 nfd)

let test_regexp () =
  let t0 = Str.regexp_case_fold ".*yst.*" in
  assert (Str.string_match t0 "haystack" 0);
  let t2 = Str.regexp_string_case_fold "ySt" in
  assert (2 = Str.search_forward t2 "haystack" 0);
  assert (2 = try Str.search_forward t2 "haystack" 0 with Not_found -> -1);
  assert (-1 = try Str.search_forward t2 "hay_stack" 0 with Not_found -> -1);
  assert (0 <= try Str.search_forward t2 "haystack" 0 with Not_found -> -1)

(*
  Assrt.equals_int __LOC__ 5 (List.length c0.url_cleaner);
  Assrt.equals_int __LOC__ 2 (List.length c0.posse)
*)

let test_needle () =
  let t = Str.regexp_case_fold ".*yst.*" in
  Assrt.equals_int __LOC__ 1 (Search.string_rank t "haystack")

let test_needles () =
  let tp = [ "föo"; "bär"; "báz" ] |> Search.needles_prepare in
  ("my fÓo", "", "") |> Search.entry_rank tp |> Assrt.equals_int __LOC__ 2

let test_emoji () =
  "my 😷 ö" |> Ubase.from_utf8 |> Assrt.equals_string __LOC__ "my 😷 o"

let () =
  Unix.chdir "../../../test/";
  test_ubase ();
  test_regexp ();
  test_needle ();
  test_needles ();
  test_emoji ()
