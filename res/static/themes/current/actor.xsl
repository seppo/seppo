<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/1999/REC-xslt-19991116
  https://www.w3.org/TR/1999/REC-xpath-19991116
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:as="https://www.w3.org/ns/activitystreams#"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:ldp="http://www.w3.org/ns/ldp#"
  xmlns:math="http://exslt.org/math"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:schema="http://schema.org#"
  xmlns:sec="https://w3id.org/security#"
  xmlns:seppo="http://seppo.mro.name/2023/ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="as ldp math rdf schema sec seppo"
  version="1.0">

  <!-- xsl:strip-space elements="seppo:isBlocked seppo:isFollowed" / -->

  <!-- replace linefeeds with <br> tags -->
  <xsl:template name="linefeed2br">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="'&#10;'"/>
    <xsl:choose>
      <xsl:when test="contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/><br class="br"/><!-- xsl:comment>Why do we see 2 br on Safari and output/@method=html here? http://purl.mro.name/safari-xslt-br-bug</xsl:comment -->
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base_absolute" select="/*/@xml:base"/>
  <xsl:variable name="xml_base"          select="normalize-space($xml_base_absolute)"/>
  <xsl:variable name="xml_base_pub"      select="concat($xml_base,'/o')"/>
  <xsl:variable name="skin_base"         select="concat($xml_base,'/themes/current')"/>
  <xsl:variable name="cgi_url"          select="concat($xml_base,'seppo.cgi')"/>

  <xsl:template name="banner-input">
    <xsl:param name="id"/>
    <xsl:param name="value"/>
    <xsl:param name="txt"/>
    <xsl:param name="disabled"/>
    <label for="{$id}" role="button">
      <xsl:if test="'pending' = $value">
        <xsl:attribute name="class">pending</xsl:attribute>
      </xsl:if>
      <xsl:if test="$disabled">
        <xsl:attribute name="aria-disabled">true</xsl:attribute>
      </xsl:if>
      <input type="checkbox" name="{$id}" id="{$id}">
  <!--
        <xsl:if test="$disabled">
          <xsl:attribute name="disabled">disabled</xsl:attribute>
        </xsl:if>
  -->
        <xsl:if test="'no' != $value">
          <xsl:attribute name="checked">checked</xsl:attribute>
        </xsl:if>
      </input>
    <span class="if-pending" title="pending"> ⏳ </span>
    <xsl:copy-of select="$txt"/>
    </label>
  </xsl:template>

  <xsl:template match="/rdf:RDF">
    <html xmlns="http://www.w3.org/1999/xhtml" data-xml-base-pub="{$xml_base_pub}" class="script-inactive" bgcolor="darkgreen">
      <xsl:for-each select="*[as:id and ldp:inbox and as:outbox and as:preferredUsername]">
        <xsl:variable name="host1" select="as:id/@rdf:resource"/>
        <xsl:variable name="host0" select="substring-after($host1,'://')"/>
        <xsl:variable name="host" select="substring-before($host0,'/')"/>
        <head>
          <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
          <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
          <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css"/>
          <link href="{$skin_base}/actor.css" rel="stylesheet" type="text/css"/>
          <link rel="icon" type="image/jpg" href="{$xml_base}/me-avatar.jpg"/>

          <title><xsl:value-of select="as:name"/></title>
        </head>
        <body>
          <div id="banner">
            <div id="banner-img">
              <img alt="Banner" src="{as:image[1]/as:Image[1]/as:url[1]/@rdf:resource}"/>
            </div>
            <form name="actor" action="actor.xml" method="post">
              <xsl:variable name="subscriber" select="normalize-space(../*/seppo:is_subscriber)"/>
              <xsl:variable name="subscribed" select="normalize-space(../*/seppo:am_subscribed_to)"/>
              <xsl:variable name="blocked" select="normalize-space(../*/seppo:is_blocked)"/>
              <xsl:variable name="token" select="normalize-space(../*/seppo:token)"/>
              <input type="hidden" name="token" value="{$token}"/>
              <input type="hidden" name="resource" value="{as:id/@rdf:resource}"/>
              <input type="hidden" name="inbox" value="{ldp:inbox/@rdf:resource}"/>
              <input type="hidden" name="~is_subscriber" value="{$subscriber}"/>
              <input type="hidden" name="~am_subscribed_to" value="{$subscribed}"/>
              <input type="hidden" name="~is_blocked" value="{$blocked}"/>

              <!-- a href="{$cgi_url}/timeline/p/" rel="start" role="button" title="Timeline">🕞</a -->
              <a href="{$cgi_url}/timeline/p/" rel="start" role="button" title="Home">🏡</a>
			  <!-- a href="{as:url/@rdf:resource}" role="button">📄 Profile</a -->
			  <a aria-disabled="true" href="" role="button">✉️ <span class="hidden-xs">Message</span></a>

<!--
              <label class="pending" title="pending" for="subscribed2" role="button">
                <input type="checkbox" id="subscribed2" name="subscribed2" checked="checked"/>
                <span class="if-pending"> … </span>
                <span class="hidden-xs">Subscrubed to</span> 👂
              </label>
-->
              <xsl:call-template name="banner-input">
                <xsl:with-param name="id">am_subscribed_to</xsl:with-param>
                <xsl:with-param name="value" select="$subscribed"/>
                <xsl:with-param name="txt"><span class="hidden-xs">Subscribed to</span> 👂</xsl:with-param>
              </xsl:call-template>
              <xsl:call-template name="banner-input">
                <xsl:with-param name="id">is_subscriber</xsl:with-param>
                <xsl:with-param name="value" select="$subscriber"/>
                <xsl:with-param name="txt"><span class="hidden-xs">is Subscriber</span> 📣</xsl:with-param>
                <xsl:with-param name="disabled" select="'true'"/>
              </xsl:call-template>
              <xsl:call-template name="banner-input">
                <xsl:with-param name="id">is_blocked</xsl:with-param>
                <xsl:with-param name="value" select="$blocked"/>
                <xsl:with-param name="txt"><span class="hidden-xs">Blocked</span> 🤐</xsl:with-param>
              </xsl:call-template>

              <input class="noscript" type="submit"/>
            </form>
          </div>
          <div id="avatar">
            <xsl:for-each select="as:icon/as:Image/as:url">
              <a href="{../../../as:url/@rdf:resource}"><img id="avatar" alt="Avatar" src="{@rdf:resource}"/></a>
            </xsl:for-each>
          </div>

          <h3 id="name"><a href="{as:url/@rdf:resource}" title="name"><xsl:value-of select="as:name"/></a></h3>

          <p><a href="{as:url/@rdf:resource}" title="preferredUsername">@<span id="preferredUsername"><xsl:value-of select="as:preferredUsername"/></span>@<xsl:value-of select="$host"/></a></p>

          <p class="clickable" title="summary">
            <xsl:call-template name="linefeed2br">
              <xsl:with-param name="string" select="as:summary"/>
            </xsl:call-template>
          </p>

          <dl class="clickable">
            <xsl:for-each select="as:attachment/schema:PropertyValue[schema:value]">
              <dt><xsl:value-of select="as:name"/></dt>
              <dd><xsl:value-of select="schema:value"/></dd>
            </xsl:for-each>
          </dl>

          <script src="{$skin_base}/actor.js"></script>
        </body>
      </xsl:for-each>
    </html>
  </xsl:template>
</xsl:stylesheet>
