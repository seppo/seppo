<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/xslt-10/
  https://www.w3.org/TR/xpath-10/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="atom rdf xsl"
  version="1.0">

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base">../..</xsl:variable>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'/o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'/themes/current')"/>
  <xsl:variable name="cgi_url" select="concat($xml_base,'/seppo.cgi')"/>

  <xsl:template match="/a:feed">
	<html xmlns="http://www.w3.org/1999/xhtml" data-xml-base-pub="{$xml_base_pub}" class="logged-out script-inactive" bgcolor="darkgreen">
	    <head>
        <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <script src="{$skin_base}/subscribers.js"></script>
<!-- script src="{$xml_base}/live.js"></script -->
        <title><xsl:value-of select="a:title"/></title>
	    </head>
		<body>
		  <div id="header">
            <!-- a href="{$cgi_url}/timeline/p/" rel="start" role="button" title="Timeline">🕞 <span class="hidden-xs">Timeline </span></a -->
            <a href="{$cgi_url}/timeline/p/" rel="start" role="button" title="Home">🏡 <span class="hidden-xs">Home</span></a>

			<h1><xsl:value-of select="a:title"/></h1>
			<form id="webfinger" method="get" action="../../seppo.cgi/activitypub/actor.xml"><label for="resource">Look up somebody:</label><input type="text" name="resource" placeholder="@alice@example.org"/></form>
            <xsl:call-template name="pagination">
              <xsl:with-param name="id">pagination-lower</xsl:with-param>
            </xsl:call-template>
 		  </div>
			<xsl:variable name="entries" select="a:link[@rel='subscribers' or @rel='subscribed_to' or @rel='blocked']"/>
			<xsl:if test="count($entries) &lt; 1">
			  <p>Here will show up a paged list of accounts. There are none until you pick some, however.</p>
			</xsl:if>
			<form id="subscribers" method="post" action="{$cgi_url}/subscribers">
				<input type="submit" class="noscript" value="Send"/>
				<ol class="actors" role="main">
					<xsl:for-each select="$entries">
						<li class="actor">
							<input type="checkbox" name="{@title}" checked="checked" disabled="disabled"/>
							<img alt="Avatar" src="{$cgi_url}/activitypub/actor.xml/icon?resource={@href}"/>
			  				  <xsl:value-of select="@title"/>
							  <xsl:text> </xsl:text>
							  <xsl:choose>
						        <xsl:when test='@rfc7565'>
									<a href="{$cgi_url}/{@rfc7565}">
										<xsl:value-of select="substring-after(@rfc7565,'acct:')"/>
									</a>
						        </xsl:when>
						        <xsl:otherwise>
									<a href="{$cgi_url}/activitypub/actor.xml?resource={@href}">
										<xsl:value-of select="@href"/>
									</a>
						        </xsl:otherwise>
					          </xsl:choose>
						</li>
					</xsl:for-each>
				</ol>
				<input type="submit" class="noscript" value="Send"/>
			</form>
            <xsl:call-template name="pagination">
              <xsl:with-param name="id">pagination-upper</xsl:with-param>
            </xsl:call-template>
		</body>
    </html>
  </xsl:template>

  <xsl:template name="pagination">
    <xsl:param name="id">-</xsl:param>
    <xsl:if test="a:link[@rel='previous'] or a:link[@rel='next']">
      <xsl:variable name="xml_base_relative"/>
      <xsl:variable name="self" select="a:link[@rel='self']"/>
    <ol id="{$id}" class="noshade" role="navigation" aria-label="pagination">
      <li>
        <xsl:variable name="link" select="a:link[@rel='first']"/>
        <a title="most recent page" href="{$xml_base_relative}{$link/@href}#pagination-upper" rel="first" role="button"><xsl:value-of select="$link/@title"/></a>
      </li>
      <li>
        <a title="more recent page" rel="previous" role="button">
          <xsl:variable name="link" select="a:link[@rel='previous']"/>
          <xsl:choose>
          <xsl:when test="$link/@href">
            <xsl:attribute name="href"><xsl:value-of select="concat($xml_base_relative, $link/@href, '#pagination-lower')"/></xsl:attribute>
            <xsl:attribute name="class">probe-2xx</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="aria-disabled">true</xsl:attribute>
          </xsl:otherwise>
          </xsl:choose>
        &#160;<xsl:value-of select="$link/@title"/>&#160;</a>
      </li>
      <li aria-current="page">
        <a href="{$xml_base_relative}{$self/@href}#{$id}" title="current page" rel="self"><xsl:value-of select="$self/@title"/></a>
      </li>
      <li>
        <a title="next older page" rel="next" role="button">
          <xsl:variable name="link" select="a:link[@rel='next']"/>
          <xsl:choose>
          <xsl:when test="$link/@href">
            <xsl:attribute name="href"><xsl:value-of select="concat($xml_base_relative, $link/@href, '#pagination-upper')"/></xsl:attribute>
            <xsl:attribute name="class">probe-2xx</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="aria-disabled">true</xsl:attribute>
          </xsl:otherwise>
          </xsl:choose>
        &#160;<xsl:value-of select="$link/@title"/>&#160;</a>
      </li>
      <li>
        <xsl:variable name="link" select="a:link[@rel='last']"/>
        <a title="oldest page" href="{$xml_base_relative}{$link/@href}#pagination-lower" rel="last" role="button"><xsl:value-of select="$link/@title"/></a>
      </li>
    </ol>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
