
function autoButtons(elms) {
  console.log('actor.js:autoButtons ' + elms.length);
  for (var i = elms.length - 1; i >= 0 ; i--) {
    const elm = elms[i];
    // https://stackoverflow.com/a/26946264/349514
    elm.addEventListener('input', function (evt) {
      this.form.submit();
    });
  }
}

document.documentElement.classList.add('script-active');
document.documentElement.classList.remove('script-inactive');
const xml_base_pub = document.documentElement.getAttribute("data-xml-base-pub");
// make http and geo URIs (RFC 5870) clickable + microformat
function clickableTextLinks(elmsRendered) {
  console.log('actor.js:clickableTextLinks');
  for (var i = elmsRendered.length - 1; i >= 0 ; i--) {
    const elm = elmsRendered[i];
    elm.innerHTML = elm.innerHTML
      .replace(/(https?:\/\/([^ \t\r\n"'<]+[^ \t\r\n"'<.,;:)]))/gi, '<a rel="noreferrer" title="WWW" class="http" href="$1">$2</a>')
      // https://alanstorm.com/url_regex_explained/ \b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))
      // .replace(/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/gi, '<a rel="noreferrer" class="http" href="$1">$1</a>')
      .replace(/@([a-z0-9._-]+)@([a-z0-9._-]+)/gi, '<a rel="webfinger" title="Webfinger" class="http" href="https://$2/.well-known/webfinger?resource=acct:$1@$2">@$1@$2</a>')
      .replace(/geo:(-?\d+.\d+),(-?\d+.\d+)(\?z=(\d+))?/gi, '<a class="geo" href="https://opentopomap.org/#marker=12/$1/$2" title="zoom=$4">geo:<span class="latitude">$1</span>,<span class="longitude">$2</span>$3</a>')
      .replace(/(#RFC(\d+)(#\S*[0-9a-z])?)/gi, '<a class="rfc" href="https://tools.ietf.org/html/rfc$2$3" title="RFC $2">$1</a>')
      .replace(/(urn:ietf:rfc:(\d+)(#\S*[0-9a-z])?)/gi, '<a class="rfc" href="https://tools.ietf.org/html/rfc$2$3" title="RFC $2">$1</a>')
      .replace(/(urn:isbn:([0-9-]+)(#\S*[0-9a-z])?)/gi, '<a class="isbn" href="https://de.wikipedia.org/wiki/Spezial:ISBN-Suche?isbn=$2" title="ISBN $2">$1</a>')
      .replace(/(urn:ean:([0-9-]+)(#\S*[0-9a-z])?)/gi, '<a class="ean" href="https://www.ean-suche.de/?q=$2" title="EAN $2">$1</a>')
      .replace(/(CVE-[0-9-]+-[0-9]+)/gi, '<a class="cve" href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=$1">$1</a>');
  }
}

autoButtons(document.querySelectorAll("[role='button'] input[type='checkbox']"));
clickableTextLinks(document.getElementsByClassName('clickable'));
