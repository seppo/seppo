    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


./d/       posts per day, indices into ./p.s
./m/       media, e.g. images
./p/       paged posts, indices into ./p.s
./t/       pages posts per tag, indices into ./p.s
./id.cdb   id -> indices into ./p.s
./p.s      append-only store of all public posts
./url.cdb  url -> id
