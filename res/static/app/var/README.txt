    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


./cache/inbox/     temporary, incoming, e.g. notes from subcribed to actors
./db/              all(!) the actual, owner generated, content – posts, media,
./lib/
./log/seppo.log    logging (size limited to 10MB),
./log/seppo.log.0  rolled over, previous log (overwritten on size rotation)
./run/cookie.s     random secret to encrypt session cookie,
./run/cron.run     marker last time ./seppo.cgi cron was executed,
./run/ipban.cdb    currently blocked IP addresses,
./run/queue.pid    background job pid
./spool/job/       job queue
