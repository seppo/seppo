    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


Part of a maildir[1]-inpired directory structure.

Files with ongoing non-atomic, transient write operations.

No persistent files.

[1]: https://web.archive.org/web/19971012032244/http://www.qmail.org/qmail-manual-html/man5/maildir.html
