    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


Incoming activities[1], signature[2] verified, sending actor matched, parsed[3] and serialized.

[1]: https://www.w3.org/TR/activitystreams-vocabulary/#activity-types
[2]: https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#section-2.5
[3]: https://codeberg.org/seppo/seppo/src/commit/973d3bb/as2_vocab/decode.ml#L380-L384
