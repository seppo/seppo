    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================
 

This directory holds all configuration data only written
by the admin web interface 'seppo.cgi/config/'. 

ban.s           IP ban settings: timing & retries
baseurl.s       web interface base url
passwd.s        username and password bcrypt hash
id_rsa.priv.pem personal private key, KEEP SAFE and backup
profile.s       title, bio, language, timezone, etc.
