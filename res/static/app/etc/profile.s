((5:title23:Welcome to my Seppo🌻)(3:bio901:This is your personal public page. Adjust it to your liking at

🕞 Timeline -> ⚙️ Settings -> Edit my Profile 🎭

Use it e.g. to say some introductory words about you. However, don't be overly verbose or expose sensitive details. Once published, they may be impossible to purge from the internet.

Now feel free to look up some persons at

🕞 Timeline -> 👥 People

and maybe subscribe. Or just write a post to the public. It will appear below and be sent to all your subscribers. The author of #Seppo posts as @social@mro.name.

You can tinker with all visuals (images, css styles, and xsl templates) as you like (but you don't have to), because it's just files in themes/current/ on your 🏡 webspace! Use e.g. Filezilla to modify them. Also adjust the profile images this way for now, see "Edit my Profile 🎭".

If something breaks, see 💡 https://Seppo.mro.name/sos.)(8:language2:en)(8:timezone13:Europe/Berlin)(14:posts-per-page2:50))
