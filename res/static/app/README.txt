    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


In here #Seppo! keeps all it needs to operate.

Inspect everything at will, but don't tamper or be prepared for surprises.

To facrory-reset the styling of the web-appearance:

1. delete the broken files (OUTSIDE here, NEVER delete app/etc/id_rsa.priv.pem)
2. delete the file ../delete-me-to-restore-missing
3. visit https://example.org/myseppo/seppo.cgi/search/?q=hello to restore
   missing files with factory settings.

The web interface ../seppo.cgi is mostly there to make manual changes to the
files in here unnecessary.

To reset your password, delete the file app/etc/passwd.s and
set a new one visiting http://example.org/myseppo/seppo.cgi
