    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web. https://Seppo.mro.name

Copyright (C) The #Seppo contributors. All rights reserved.
===========================================================


except for seppo.cgi and ./app/ all other files and directories can be deleted and
will be recreated with factory settings or recent content.

/.well-known/webfinger/.htaccess symlink to sibling inside ./.well-known/webfinger/
./.htaccess
./.well-known/webfinger/
./activitypub/                   public ActivityPub posts etc.
./app/                           #seppo!'s internal data. Peek, don't poke.
./contrib/                       helpers for self-hosting
./delete-me-to-restore-missing   delete for factory reset of disappeared files
./favicon.ico
./index.html
./me-avatar.jpg                  copy of app/etc/me-avatar.jpg
./me-banner.jpg                  copy of app/etc/me-banner.jpg
./o/                             the public blog content
./robots.txt
./seppo.cgi                      the #Seppo! program
./sitemap.xml
./themes/current/                the visual look
