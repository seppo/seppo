
nginx opted to not support CGIs. You need a webserver supporting them and
reverse-proxy it in case.

#Seppo enables people have their own social web server without requiring
privileged (root) server access and system administration knowledge. For
long term reliability reasons it does not depend on scripting engines. That's
possible with shared-hosting and CGIs.
