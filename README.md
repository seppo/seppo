```
    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web.

Copyright (C) The #Seppo contributors. All rights reserved.
```

# \#Seppo!

Your 🚲 for the Social Web.

* Demo https://Seppo.Social/demo
* Downloads https://Seppo.Social/downloads
* Support https://Seppo.Social/support
* Community Mailing List https://Seppo.Social/mailinglist
* (tbd: Announcements [@news@Seppo.Social](https://Seppo.Social/news/))
* Blog https://Seppo.Social/blog
* 🆘 [security@Seppo.Social](mailto:security@Seppo.Social)
* Development https://Seppo.Social/development

## Design Goals

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |      |    ×   |            |
| Reliability     |     ×     |      |        |            |
| Usability       |     ×     |      |        |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |           |  ×   |        |            |
| Portability     |           |  ×   |        |            |

## Pull Requests

There is an [issue](https://codeberg.org/Codeberg/Community/issues/1591#issuecomment-2030173)
preventing pull requests with UI support on this repositoy due to the
publication model I chose when starting the project (and my bad habit of doing
force push frequently).

I am happy to take them, however! Please

1. fork as you normally would and
2. [open an issue](https://codeberg.org/seppo/seppo/issues/new?title=Pull%20Request:+&content=please%20mention%20the%20git%20commit%20href%20to%20get%20it)
   and mention the git sha url you'd like to see merged,

or mail a patch to the https://Seppo.Social/mailinglist.

## Mirrors

* canonical <https://Seppo.Social/development>
* primary <https://codeberg.org/seppo/seppo>
* <https://git.sr.ht/~mro/seppo>
* <https://repo.or.cz/Seppo.git>
* <https://notabug.org/mro/seppo>
* <https://codeberg.org/mro/seppo>
* <https://code.mro.name/mro/seppo>

---

Generously supported by [NLNet grant 2022-08-141 from the 🇪🇺 NGI0 Entrust
fund](https://nlnet.nl/project/Seppo/)

