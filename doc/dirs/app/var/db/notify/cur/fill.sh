#!/in/sh
set -e

cd "$(dirname "$0")"

body () {
  v0="id"
  l0=$((2))
  v1="$1"
  l1=$(printf %s "$1" | wc -c)
  l1=$((l1))
  printf "(%d:%s%d:%s)" "$l0" "$v0" "$l1" "$v1"
}

readonly url="https://bewegung.social/users/mro"

for i in $(seq 0 9999);
do
  u="$url#$i"
  # base64url https://www.rfc-editor.org/rfc/rfc4648#section-5
  f="$(printf "%s" "$u" | openssl dgst -binary -sha1 | openssl base64 -A | tr '+/=' '-_.' )"
  body "$u" > "base64url_${f}s" 
done
beep