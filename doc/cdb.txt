
read_uiui_le

add_entry cdb (key val)
  if the file is empty
    write 2048 zero bytes
  else if the file has set p[0] > 2048
    set p[0..255] to 2048 zero bytes
    chop it off at p[0]
  else
    fseek eof
  append key val

do_index cdb
  if the file has set p[0] > 2048
    chop it off
    set p[0..255] to 2048 zero bytes
  else
    fseek 2048
  walk all records – from 2048 to eof
    feed a map with key -> pos

    or maybe a map of maps: hash%256 -> key -> pos

  0..255 each
    write pos and count to p[n]
    sort acc ((hash / 256) % count), hash, key
    



http://cr.yp.to/cdb/cdb.txt
https://packages.debian.org/buster/tinycdb
https://github.com/dustin/snippets/blob/master/ocaml/lib/cdb.ml