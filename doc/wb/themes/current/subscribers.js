document.documentElement.classList.add('script-active');
document.documentElement.classList.remove('script-inactive');
const xml_base_pub = document.documentElement.getAttribute("data-xml-base-pub");

function probeHref(elms) {
  //console.log("probeHref");
  for (let a of elms) {
    console.debug("probeHref: %s", a.href);
    if(a.getAttribute("aria-disabled"))
      // disabled a priori
      return;
    fetch(a.href)
    .then(function(response) {
      a.classList.remove("probe-2xx");
      if (response.ok)
        return;
      a.setAttribute("aria-disabled",true);
    })
    .catch(function(err) {
      console.error("%s: %s", a.href, err);
    });
  }
}

// Firefox 56+ doesn't fire that one in xslt situation: document.addEventListener("DOMContentLoaded", function(event) { console.log("DOM fully loaded and parsed"); });
var addlink;
document.onreadystatechange = function () {
  if(addlink !== undefined)
    return;
  probeHref(document.getElementsByClassName("probe-2xx"));
}
