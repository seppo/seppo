
function diff(t0,t1) {
  return ((t1 - t0) / 1000).toFixed(0);
}

document.getElementById("q_lock_age").innerText = diff( Date.parse(document.getElementById("q_lock").innerText), Date.now());
