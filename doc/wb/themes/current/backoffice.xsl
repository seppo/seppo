<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/1999/REC-xslt-19991116
  https://www.w3.org/TR/1999/REC-xpath-19991116
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:b="http://seppo.mro.name/2023/backoffice#"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:schema="http://www.w3.org/2001/XMLSchema#"
  xmlns:seppo="http://seppo.mro.name/2023/ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="date h rdf schema seppo"
  version="1.0">

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base">../../</xsl:variable>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'/o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'/themes/current')"/>
  <xsl:variable name="cgi_url" select="concat($xml_base,'seppo.cgi')"/>

  <xsl:template match="/rdf:RDF/rdf:Description[@rdf:about='']">
    <html xmlns="http://www.w3.org/1999/xhtml" class="logged-in" bgcolor="darkgreen">
    <head>
      <meta name="generator" content="HTML Tidy for HTML5 for FreeBSD version 5.8.0" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css" />
      <title>⚙️ Settings</title>
    </head>
    <body>
      <noscript><p>JavaScript deactivated, fully functional, but <em>nicer</em> if on.</p></noscript>
      <div aria-label="Header" id="header">
        <!-- a href="{$cgi_url}/timeline/p/" rel="start nofollow" role="button">🕞 <span class="hidden-xs">Timeline</span></a -->
        <a href="{$cgi_url}/timeline/p/" rel="start" role="button" title="Home">🏡 <span class="hidden-xs">Home</span></a>
      </div>
      <div class="container" role="main">
        <h1>⚙️ Settings</h1>
        <h2 id="tools">Tools</h2>
        <ul class="nobullet">
          <li><a href="{$cgi_url}/profile" role="button">Edit my Profile 🎭</a></li>
          <li><a href="{$cgi_url}/passwd" role="button">Change Password ㊙️</a></li>
        </ul>
        <h2 id="machine">Machine Room</h2>
        <form id="http" method="get" action="{$cgi_url}/http">
          <input type="url" name="get" placeholder="signed http GET"/>
        </form>
        <h2 id="queues">Queues</h2>
        <ul>
          <li><tt>app/var/run/queue.pid</tt>:
           <span id="q_lock"><xsl:value-of select="b:q_lock"/></span> ~ age <span id="q_lock_age">~</span>s</li>
        </ul>
        <h3 id="job">Job</h3>
        <ul>
          <li><tt>app/var/spool/job/cur</tt>:   <xsl:value-of select="b:spool_job_cur"/></li>
          <li><tt>app/var/spool/job/err</tt>:   <xsl:value-of select="b:spool_job_err"/></li>
          <li><tt>app/var/spool/job/new</tt>:   <xsl:value-of select="b:spool_job_new"/></li>
          <li><tt>app/var/spool/job/run</tt>:   <xsl:value-of select="b:spool_job_run"/></li>
          <li><tt>app/var/spool/job/tmp</tt>:   <xsl:value-of select="b:spool_job_tmp"/></li>
          <li><tt>app/var/spool/job/wait</tt>:  <xsl:value-of select="b:spool_job_wait"/></li>
        </ul>
        <h3 id="inbox">Inbox</h3>
        <ul>
          <li><tt>app/var/cache/inbox/cur</tt>: <xsl:value-of select="b:cache_inbox_cur"/></li>
          <li><tt>app/var/cache/inbox/new</tt>: <xsl:value-of select="b:cache_inbox_new"/></li>
        </ul>
        <h2 id="federation">Environment &amp; Federation</h2>
        <ul>
          <li><a href="{b:x509_pem_url}">id_rsa.pub.pem</a></li>
          <ul>
          <li>id: <xsl:value-of select="b:x509_id"/></li>
              <li>fingerprint: <xsl:value-of select="b:x509_fingerprint"/></li>
          </ul>
        </ul>
        <h2 id="standards">Standards</h2>
        <dl>
          <dt>"The Internet is for End Users" (<a href="https://www.rfc-editor.org/rfc/rfc8890.html">RFC8890</a>)</dt>
          <dd></dd>
          <dt>security.txt (<a href="https://www.rfc-editor.org/rfc/rfc9116">RFC9116</a>)</dt>
          <dd><a href="/.well-known/security.txt">/.well-known/security.txt</a></dd>
          <dt>WebFinger (<a href="https://www.rfc-editor.org/rfc/rfc7033.html">RFC7033</a>, <a href="https://www.rfc-editor.org/rfc/rfc7565.html">RFC7565</a>)</dt>
          <dd><a rel="webfinger" href="https://seppo.mro.name/.well-known/webfinger?resource=acct:demo@seppo.mro.name">https://seppo.mro.name/.well-known/webfinger?resource=acct:demo@seppo.mro.name</a></dd>
          <dt><a href="https://w3.org/TR/activitystreams-core">ActivityStreams 2.0 (json
          flavour)</a></dt>
          <dt><a href="https://w3.org/TR/activitypub/">ActivityPub</a></dt>
          <dd><a href="https://www.w3.org/TR/activitypub/#actor-objects">Actor</a>
           <xsl:text> </xsl:text>
           <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-person">Person</a>,
          <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-create">Create</a>/<a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-delete">Delete</a>
           <xsl:text> </xsl:text>
           <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-note">Note</a>,
          <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-follow">Follow</a>,
          <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-undo">Undo</a>
          </dd>
          <dt>Signing HTTP Messages (<a href=
          "https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12">draft-cavage-http-signatures-12</a>)</dt>
          <dd>all requests are signed with an initially generated RSA key. Rotating may break followers etc.
          See <a href="../../activitypub/actor.jsa#publicKey">the public key PEM</a> embedded in the
          profile..</dd>
          <dt>The Common Gateway Interface (CGI) (<a href="https://www.rfc-editor.org/rfc/rfc3875.html">RFC3875</a>)</dt>
          <dd>generate static files for reading, listen to publishers (subscribed to), notify subscribers</dd>
          <dt>Atom Feed (<a href="https://www.rfc-editor.org/rfc/rfc4287">RFC4287</a>, <a href=
          "https://www.rfc-editor.org/rfc/rfc4685">RFC4685</a> &amp; <a href=
          "https://www.rfc-editor.org/rfc/rfc5005">RFC5005</a>)</dt>
          <dd><a href="../../o/p/index.xml">o/p/index.xml</a></dd>
          <dt><a href="https://www.w3.org/TR/xslt-10/">XSLT 1.0</a></dt>
          <dd>turn the atom feeds into xhtml (client-side, without javascript)</dd>
          <dt><a href="http://www.w3.org/TR/xhtml1/">XHTML 1.1 strict</a></dt>
          <dd>robust page rendering</dd>
          <dt><a href="https://www.w3.org/TR/CSS/">CSS</a></dt>
          <dd>layout and (<a href="https://oklch.com/#79.27,0.171,70.67,100">OKLCH</a>) colors, dark mode</dd>
          <dt><a href="https://projects.verou.me/awesomplete/">awesomeplete</a></dt>
          <dt><a href="http://cr.yp.to/cdb/cdb.txt">cdb</a></dt>
          <dd>constant database for lookups</dd>
          <dt><a href="https://en.wikipedia.org/wiki/Canonical_S-expressions">canonical
          s-expressions</a></dt>
          <dd>config &amp; data (except the cdbs). <a href="https://cr.yp.to/qmail/guarantee.html">"Don't parse" (djb)</a>.
          Csexp are much like <a href="https://cr.yp.to/proto/netstrings.txt">netstrings</a></dd>
          <dt>Web Host Metadata (<a href=
          "https://www.rfc-editor.org/rfc/rfc6415.html">RFC6415</a>)</dt>
          <dd>not implemented, webfinger endpoint is fixed. Misskeyism.</dd>
          <dt><a href="https://nodeinfo.diaspora.software/">NodeInfo</a></dt>
          <dd>not implemented, no functional benefits, just server statistics, Diasporism.</dd>
        </dl>
        <h2 id="software">Software</h2>
        <dl>
          <dt><a href="https://Seppo.mro.name">Seppo.mro.name</a></dt>
          <dt><a href="https://seppo.mro.name/support/">Seppo.mro.name/support</a></dt>
          <dt><a href="https://seppo.mro.name/downloads/">Seppo.mro.name/downloads</a></dt>
          <dt><a href="https://seppo.mro.name/development/">Seppo.mro.name/development</a></dt>
        </dl>
      </div>
      <script src="{$skin_base}/backoffice.js"></script>
    </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
