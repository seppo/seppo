<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://w3.org/TR/xslt-10
  https://w3.org/TR/xpath-10
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:as="https://www.w3.org/ns/activitystreams#"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:ldp="http://www.w3.org/ns/ldp#"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:schema="http://schema.org#"
  xmlns:seppo="http://seppo.mro.name/2023/ns#"
  xmlns:toot="http://joinmastodon.org/ns#"
  xmlns:wf="urn:ietf:rfc:7033"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="a as opensearch ldp media georss rdf"
  version="1.0">

  <xsl:include href="posts.xsl"/>
</xsl:stylesheet>
