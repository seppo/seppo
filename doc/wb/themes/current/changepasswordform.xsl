<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/xslt-10/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/h:html">
    <xsl:variable name="xml_base">../</xsl:variable>
    <xsl:variable name="xml_base_pub" select="concat($xml_base,'o')"/>
    <xsl:variable name="skin_base" select="concat($xml_base,'themes/current')"/>
    <xsl:variable name="cgi_base" select="concat($xml_base,'seppo.cgi')"/>
    <html xmlns="http://www.w3.org/1999/xhtml" bgcolor="darkgreen">
      <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" type="image/jpg" href="../me-avatar.jpg"/>

        <title><xsl:value-of select="h:head/h:title"/></title>
      </head>
      <body>
        <xsl:for-each select="h:body/h:form">
          <label form="{@name}" class="h1"><xsl:value-of select="../../h:head/h:title"/></label>
          <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
          </xsl:copy>
        </xsl:for-each>
        <p><img
          width="600px" height="100px"
          alt="../app/ access permission check: ok (https://Seppo.mro.name/S1005)"
          title="if a security alert is visible here, ../app/ is publicly accessible from the www. See https://Seppo.mro.name/S1005"
          src="../app/i-must-be-403.svg"/></p>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="h:input">
    <xsl:if test="@type != 'hidden'">
      <label for="{@name}" class="h3"><xsl:value-of select="@name"/></label>
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
