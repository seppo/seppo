<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
  xmlns:thr="http://purl.org/syndication/thread/1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="a media opensearch thr"
  xmlns:math="http://exslt.org/math"
  extension-element-prefixes="math"
  version="1.0">

  <!-- xsl:variable name="redirector">https://anonym.to/?</xsl:variable --> <!-- mask the HTTP_REFERER -->
  <xsl:variable name="redirector"></xsl:variable>
  <xsl:variable name="archive">https://web.archive.org/web/</xsl:variable>

  <!-- replace linefeeds with <br> tags -->
  <xsl:template name="linefeed2br">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="'&#10;'"/>
    <xsl:choose>
      <xsl:when test="contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/><br class="br"/><xsl:comment> Why do we see 2 br on Safari and output/@method=html here? http://purl.mro.name/safari-xslt-br-bug </xsl:comment>
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="calculate-day-of-the-week">
    <!-- https://www.oreilly.com/library/view/xslt-cookbook/0596003722/ch03s02.html -->
    <xsl:param name="date-time"/>
    <xsl:param name="date" select="substring-before($date-time,'T')"/>
    <xsl:param name="year" select="substring-before($date,'-')"/>
    <xsl:param name="month" select="substring-before(substring-after($date,'-'),'-')"/>
    <xsl:param name="day" select="substring-after(substring-after($date,'-'),'-')"/>
    <xsl:variable name="a" select="floor((14 - $month) div 12)"/>
    <xsl:variable name="y" select="$year - $a"/>
    <xsl:variable name="m" select="$month + 12 * $a - 2"/>
    <xsl:value-of select="($day + $y + floor($y div 4) - floor($y div 100) + floor($y div 400) + floor((31 * $m) div 12)) mod 7"/>
  </xsl:template>

  <xsl:template name="human_time">
    <xsl:param name="time">-</xsl:param>
    <xsl:variable name="wday">
      <xsl:call-template name="calculate-day-of-the-week">
        <xsl:with-param name="date-time" select="$time"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="0 = $wday">Sun</xsl:when>
      <xsl:when test="1 = $wday">Mon</xsl:when>
      <xsl:when test="2 = $wday">Tue</xsl:when>
      <xsl:when test="3 = $wday">Wed</xsl:when>
      <xsl:when test="4 = $wday">Thu</xsl:when>
      <xsl:when test="5 = $wday">Fri</xsl:when>
      <xsl:when test="6 = $wday">Sat</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="substring($time, 9, 2)"/><xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mar</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">May</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Oct</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dec</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose><xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 4)"/><xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 12, 5)"/><!-- xsl:text> Uhr</xsl:text -->
  </xsl:template>

  <xsl:template name="degrees">
    <xsl:param name="num" select="0"/>
    <xsl:choose>
      <xsl:when test="$num &lt; 0">-<xsl:call-template name="degrees"><xsl:with-param name="num" select="-$num"/></xsl:call-template></xsl:when>
      <xsl:when test="$num &gt;= 0">
        <xsl:variable name="deg" select="floor($num)"/>
        <xsl:variable name="min" select="floor(($num * 60) mod 60)"/>
        <xsl:variable name="sec" select="format-number((($num * 36000) mod 600) div 10, '0.0')"/>
        <xsl:value-of select="$deg"/>° <!--
        --><xsl:value-of select="$min"/>' <!--
        --><xsl:value-of select="$sec"/>"
      </xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <!-- http://stackoverflow.com/a/16328207 -->
  <xsl:key name="CategorY" match="a:entry/a:category" use="@term" />

  <xsl:variable name="self" select="/*/a:link[@rel = 'self']/@href"/>
  <xsl:variable name="xml_base_absolute" select="/*/@xml:base"/>
  <!-- a bit hairy, but actually works -->
  <xsl:variable name="xml_base_relative">../../<xsl:choose>
      <xsl:when test="'shaarligo.cgi/search/?q=' = substring($self, 1, 24)"/>
      <xsl:when test="'//' = translate($self, 'abcdefghijklmnopqrstuvwxyz0123456789-', '')"/>
      <xsl:otherwise>../</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="xml_base" select="normalize-space($xml_base_relative)"/>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'themes/current')"/>
  <xsl:variable name="cgi_base" select="concat($xml_base,'shaarligo.cgi')"/>

  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" data-xml-base-pub="{$xml_base_pub}">
    <head>
      <link href="style.css" rel="stylesheet" type="text/css" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="generator" content="HTML Tidy for HTML5 for FreeBSD version 5.8.0" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <script src="../live.js"></script>
      <title>Threaded Atom Feeds RFC 4685</title>
    </head>

    <body>
      <xsl:apply-templates select="a:feed"/>
    </body>
    </html>
  </xsl:template>

  <xsl:template match="a:feed">
    <h1><xsl:value-of select="a:title"/></h1>
    <ol class="odd" data-level="1">
      <!-- level=0 is reserved for the primary source -->
      <xsl:apply-templates select="a:entry[not(thr:in-reply-to)]">
        <!-- entries pointing to undefined ids are lost -->
        <xsl:with-param name="level" select="1 + 1"/>
      </xsl:apply-templates>
    </ol>
  </xsl:template>

  <xsl:template match="a:entry">
    <xsl:param name="level" />
    <xsl:variable name="entry_published" select="a:published"/>
    <xsl:variable name="entry_published_human"><xsl:call-template name="human_time"><xsl:with-param name="time" select="$entry_published"/></xsl:call-template></xsl:variable>
    <li>
      <p>
        <img class="avatar" alt="Avatar" src= "{a:author/a:icon}" />
        <a href="{a:id}" data-rfc3339="{a:published}" title="{a:published}">
          <xsl:value-of select="$entry_published_human"/>
        </a>
        <span class="name"><xsl:value-of select="a:author/a:name"/></span>
        <a href="{a:author/a:uri}" data-rfc7033="{a:author/a:email}"><xsl:value-of select="a:author/a:email"/></a>
      </p>
      <p class="plaintext">
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="a:content"/>
        </xsl:call-template>
      </p>
      <xsl:variable name="me" select="a:id"/>

      <xsl:variable name="clz">
        <xsl:choose>
          <xsl:when test="$level mod 2 = 0">even</xsl:when>
          <xsl:otherwise>odd</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="more" select="following-sibling::a:entry[thr:in-reply-to/@ref = $me]"/>

	  <xsl:if test="count($more) > 0">
        <ol class="{$clz}" data-level="{$level}">
          <xsl:apply-templates select="$more">
            <xsl:with-param name="level" select="$level + 1"/> 
          </xsl:apply-templates>
        </ol>
	  </xsl:if>
    </li>
  </xsl:template>
</xsl:stylesheet>
