#!/usr/bin/env dash
cd "$(dirname "$0")"

for f in \
  themes/current/actor.css \
  themes/current/actor.xsl \
  themes/current/posts.xsl \
  themes/current/style.css \
; do
  curl --output $f -L https://seppo.social/demo/$f
done
