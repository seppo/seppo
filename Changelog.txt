
# v0.3, Thu Oct 19 10:54:40 CEST 2023

Download: https://Seppo.mro.name/Linux-x86_64-0.3
Binary:   https://Seppo.mro.name/Linux-x86_64-0.3/seppo.cgi
Source:   https://Seppo.mro.name/Linux-x86_64-0.3/source.tar.gz
Git:      https://Seppo.mro.name/v/5f078d9
Install:  https://seppo.mro.name/en/support/#installation

Changes

- post notes
- being subscribed to (aka 'followed')
- distribute post to subscribers
- job queue to do so
- housekeeping UX (password, profile page, timezone)


Corresponds to https://blog.mro.name/2022/12/nlnet-seppo/#4-new-post-via-web-interface
and parts of https://blog.mro.name/2022/12/nlnet-seppo/#7-activitypub-activities-un-follow-block

Example to verify the signature with openssl:

$ curl -LO https://Seppo.mro.name/seppo.pub.pem
$ openssl dgst -verify seppo.pub.pem -keyform PEM -sha256 -signature seppo.cgi.sign -binary seppo.cgi

# v0.2, Tue Jun 20 16:30:35 CEST 2023

- set up a new instance via web
- name resolveable via webfinger (from e.g. mastodon digitalcourage.social/@mro)

Corresponds to https://blog.mro.name/2022/12/nlnet-seppo/#3-new-instance-via-web-interface

# v0.1, Thu Feb  9 09:30:06 CET 2023

- commandline binary seppo.cgi
- show help with $ ./seppo.cgi -h
- create new instance and account visible via webfinger @alice@example.org
- handle common error scenarios
- actionable advice on Seppo.mro.name/support

Corresponds to https://blog.mro.name/2022/12/nlnet-seppo/#1-new-instance-via-commandline-interface-cli
